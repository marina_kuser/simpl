## begin fhead
#
# funcname "otsu_threshold"
# name "Otsu thresholding"
# desc "Otsu's thresholding method involves iterating through all the possible threshold values and calculating a measure of spread for the pixel levels each side of the threshold, i.e. the pixels that either fall in foreground or background. The aim is to find the threshold value where the sum of foreground and background spreads is at its minimum."
#
## end fhead

import numpy as np
import cv2

def otsu_threshold(img, args): 
    pixel_number = img.shape[0] * img.shape[1]

    if len(img.shape)==3:
        temp=img.astype(np.float32)
        g=0.0722*temp[:,:,0]+0.7152*temp[:,:,1]+0.2126*temp[:,:,2] 
    else:
        g=img
    g = g.astype(np.uint8)

    mean_weigth = 1.0/pixel_number
    his, bins = np.histogram(g, np.arange(0,257))
    final_thresh = -1
    final_value = -1
    intensity_arr = np.arange(256)

    for t in bins[1:-1]: # This goes from 1 to 254 uint8 range (Pretty sure wont be those values)
        pcb = np.sum(his[:t])
        pcf = np.sum(his[t:])
        Wb = pcb * mean_weigth
        Wf = pcf * mean_weigth

        mub = np.sum(intensity_arr[:t]*his[:t]) / float(pcb)
        muf = np.sum(intensity_arr[t:]*his[t:]) / float(pcf)
        #print mub, muf
        value = Wb * Wf * (mub - muf) ** 2

        if value > final_value:
            final_thresh = t
            final_value = value
    
    final_img = g.copy()
    print(final_thresh)
    final_img[g > final_thresh] = 255
    final_img[g < final_thresh] = 0
    
    return final_img