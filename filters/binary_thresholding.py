## begin fhead
#
# funcname "binary_threshold"
# name "Binary thresholding"
# desc "Threshold creates a binary image from a gray image based on the threshold values"
# arg "threshold" spinbox int [0, 255] "Threshold"
#
## end fhead

import cv2
import numpy as np

def binary_threshold(img, args):
    height = img.shape[0]
    width = img.shape[1]
    threshold = args['threshold']
    
    if len(img.shape)==3:
        temp=img.astype(np.float32)
        g=0.0722*temp[:,:,0]+0.7152*temp[:,:,1]+0.2126*temp[:,:,2]
    else:
        g=img
    g = g.astype(np.uint8)
    
    for i in np.arange(height):
        for j in np.arange(width):
            a = g.item(i,j)
            if a > threshold:
                b = 255
            else:
                b = 0
            g.itemset((i,j), b)
    
    return g 