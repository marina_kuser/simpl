## begin fhead
#
# funcname "gamma_correction"
# name "Gamma Correction"
# desc "Make image brighter or darker"
# arg "gamma" checkbox "Gamma Correction"
# arg "gamma_factor" spinbox float [0, 15] "Gamma factor"
# 
## end fhead

import cv2
import numpy as np 
import math


def gamma_correction(img, args):

    gamma=args['gamma']
    gamma_factor=args['gamma_factor']

    if (gamma==True):
        
        max1=np.amax(img)
        fmax=max1.astype(np.float32)

        gf=1/gamma_factor
        slika=img/fmax
        
        slika_gamma=cv2.pow(slika,gf)             
           
        return np.uint8(slika_gamma*255)

    else:
        return img