## begin fhead
#
# funcname "adaptive_threshold"
# name "Adaptive thresholding"
# desc "The brief idea of the algorithm is that every image's pixel is set to black if its brightness is T percent lower than the average brightness of the surrounding pixels in the window of the specified size, otherwise it is set to white."
#
## end fhead

import numpy as np
import cv2

def adaptive_threshold(img, args):
    h = img.shape[0]
    w = img.shape[1]

    if len(img.shape)==3:
        temp=img.astype(np.float32)
        g=0.0722*temp[:,:,0]+0.7152*temp[:,:,1]+0.2126*temp[:,:,2] 
    else:
        g=img
    g = g.astype(np.uint8)

    S = w/8
    s2 = S/2
    T = 15.0

    #integral img
    int_img = np.zeros_like(g, dtype=np.uint32)
    for col in range(w):
        for row in range(h):
            int_img[row,col] = g[0:row,0:col].sum()

    #output img
    out_img = np.zeros_like(g)    

    for col in range(w):
        for row in range(h):
            #SxS region
            y0 = max(row-s2, 0)
            y1 = min(row+s2, h-1)
            x0 = max(col-s2, 0)
            x1 = min(col+s2, w-1)

            count = (y1-y0)*(x1-x0)

            sum_ = int_img[y1, x1]-int_img[y0, x1]-int_img[y1, x0]+int_img[y0, x0]

            if g[row, col]*count < sum_*(100.-T)/100.:
                out_img[row,col] = 0
            else:
                out_img[row,col] = 255

    return out_img