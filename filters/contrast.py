## begin fhead
#
# funcname "contrast_streching"
# name "Contrast Streching"
# desc "Fix contrast"
# arg "contrast" checkbox "Contrast"
# arg "a_min" spinbox int [0, 255] "New min value"
# arg "b_max" spinbox int [0, 255] "New max value"
# 
## end fhead

import cv2
import numpy as np 
import math
import Tkinter as tk
import tkMessageBox

def contrast_streching(img, args):

    contrast=args['contrast']
    a_min=args['a_min']
    b_max=args['b_max']

    if (contrast==True):
        slika2=img             
        min1=np.amin(img)
        max1=np.amax(img)
        if (a_min<=min1 and b_max>=max1):
            
            redak = img.shape[0]
            stupac = img.shape[1]
                        
            for i in range (0,redak):
                for j in range (0, stupac):
                    slika2[i,j]=((b_max-a_min)/(max1-min1)*(img[i,j]-min1))+a_min

        elif (a_min>min1 and b_max<max1):
            root = tk.Tk()
            root.withdraw()
            tkMessageBox.showwarning('Greska', 'Vrijednost a prevelika,a vrijednost b premala, molimo unesite nove vrijednosti!')
            root.update()

        elif (a_min>min1):
            root = tk.Tk()
            root.withdraw()
            tkMessageBox.showwarning('Greska', 'Vrijednost a prevelika, molimo unesite manju vrijednost!')
            root.update()

        elif (b_max<max1):
            root = tk.Tk()
            root.withdraw()
            tkMessageBox.showwarning('Greska', 'Vrijednost b premala, molimo unesite vecu vrijednost!')
            root.update()

        return slika2
    
    else:
        return img
